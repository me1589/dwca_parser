from sys import path as sys_path
from os.path import dirname, join as path_join
from json import load as json_load

tests_dir = dirname(__file__)
parent_dir = dirname(tests_dir)

sys_path.insert(0, parent_dir)

from dwc_parser.archive import DwcArchive

test_data = "truth_1"
archive_test_1 = path_join(tests_dir, "{}.zip".format(test_data))
truth_test_1 = path_join(tests_dir, "{}.json".format(test_data))

def test_iter_core_entries():
    with open(truth_test_1) as f:
        truth_dict = json_load(f)

    test_dwca = DwcArchive.read(archive_test_1)
    test_taxon = next(test_dwca.iter_core_entries())

    for k, v in truth_dict.items():
        assert k in test_taxon.keys()
        assert test_taxon[k] == v
