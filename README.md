# DwCA Parser
A [Darwin Core Archive](https://dwc.tdwg.org/text/) parser/writer

## Testing
Tests and cooresponding data are in [tests](./tests/)

Small archive files for manual testing are in [test_data](./test_data/)

```shell
$ coverage run -m pytest tests/ && coverage report
================================================================================= test session starts =================================================================================
platform linux -- Python 3.8.10, pytest-7.1.0, pluggy-1.0.0
rootdir: /home/edunn/repos/dwca_parser
collected 1 item                                                                                                                                                                      

tests/truth_0_test.py .                                                                                                                                                         [100%]

================================================================================== 1 passed in 0.01s ==================================================================================
Name                             Stmts   Miss  Cover
----------------------------------------------------
dwc_parser/__init__.py               0      0   100%
dwc_parser/archive/__init__.py       1      0   100%
dwc_parser/archive/_archive.py      47      8    83%
dwc_parser/archive/_section.py      55     10    82%
dwc_parser/meta/__init__.py          1      0   100%
dwc_parser/meta/_base.py            27      3    89%
dwc_parser/meta/_field.py           41     13    68%
dwc_parser/meta/_file.py           105     46    56%
dwc_parser/meta/_file_list.py       28      8    71%
dwc_parser/meta/_meta.py            40     12    70%
tests/truth_0_test.py               18      0   100%
----------------------------------------------------
TOTAL                              363    100    72%
```