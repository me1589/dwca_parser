#!/usr/bin/env python3

from argparse import ArgumentParser
from base64 import encode
from sys import stderr
from sys import path as sys_path
from os.path import exists as path_exists
from json import dumps as json_dumps
from os.path import dirname

try:
    from dwc_parser.archive import DwcArchive
except ImportError:
    sys_path.insert(0, dirname(__file__))
    from dwc_parser.archive import DwcArchive


def main():
    argparser = ArgumentParser(description="Parse a DwCA file")
    argparser.add_argument("dwc_file", help="The file to parse")
    args = argparser.parse_args()

    if not path_exists(args.dwc_file):
        print("File {} does not exist".format(args.dwc_file), file=stderr)
        exit(1)

    archive = DwcArchive.read(args.dwc_file)
    # print(json_dumps(archive._meta.to_dict(), indent=2))

    counter = 0
    for entry in archive.iter_core_entries():
        if counter > 10:
            break
        print(json_dumps(entry, indent=2))
        counter += 1


if __name__ == "__main__":
    main()