from ..meta._file import DwcFileMeta
from typing import List, Iterable
from zipfile import ZipFile
from csv import DictReader, QUOTE_MINIMAL, QUOTE_NONE
from io import TextIOWrapper
from re import sub as re_sub, fullmatch as re_fullmatch
from copy import deepcopy
from logging import getLogger

class DwcArchiveSection:
    _LOGGER = getLogger("DwcArchive")
    _REGEXP_INT = r"\d+"
    _REGEXP_FLOAT = r"[\d.]+"
    _REGEXP_INT_SUFFIX = r"_\d+"

    def __init__(self, meta: DwcFileMeta) -> None:
        self._meta = meta

    @property
    def row_type(self) -> str:
        return self._meta.rowtype

    @property
    def files(self) -> List[str]:
        return [loc for f in self._meta.files for loc in f.locations]

    @property
    def fields(self) -> List[str]:
        # They all need to be indexed, otherwise we need to return the list 
        # as-is
        sort = True
        for field in self._meta.fields:
            if field.index is None:
                sort = False
                break

        if sort:
            fields = sorted(
                self._meta.fields,
                key=lambda f: int(f.index)
            )
        else:
            fields = self._meta.fields

        return [f.term for f in fields]

    @staticmethod
    def _itis_hack(zf: ZipFile, file_path: str) -> str:
        """
            This is a hack for ITIS. Their meta.xml lists the taxa location
            as taxa.txt, but the datafile is stored as taxa_1234.txt
        """
        zf_files = zf.namelist()
        if file_path not in zf_files:
            for zf_file_path in zf_files:
                file_no_suffix = re_sub(
                    DwcArchiveSection._REGEXP_INT_SUFFIX,
                    "",
                    zf_file_path
                )
                if file_no_suffix == file_path:
                    return zf_file_path
                    
        return file_path

    def _iter_file(self, archive_path: str, file_path: str) -> Iterable[dict]:
        with ZipFile(archive_path) as zf:
            zf_path = DwcArchiveSection._itis_hack(zf, file_path)
            with zf.open(zf_path, force_zip64=True) as f:
                if self._meta.fieldsEnclosedBy == "":
                    quoting = QUOTE_NONE
                else:
                    quoting = QUOTE_MINIMAL

                # This escape is ugly, should be cleaned up. Should also 
                # recognize url-escaped characters
                csv_delimiter = self._meta.fieldsTerminatedBy.encode('raw_unicode_escape').decode('unicode_escape')
                csv_line_terminator = self._meta.linesTerminatedBy.encode('raw_unicode_escape').decode('unicode_escape')

                reader = DictReader(
                    TextIOWrapper(f, encoding=self._meta.encoding, newline=''),
                    fieldnames=self.fields,
                    quoting=quoting,
                    quotechar=self._meta.fieldsEnclosedBy,
                    delimiter=csv_delimiter,
                    lineterminator=csv_line_terminator,
                    strict=True
                )

                # Skip any header lines defined in the file lines
                for _ in range(int(self._meta.ignoreHeaderLines)):
                    next(reader)
                
                for row_idx, row in enumerate(reader):
                    row_transformed = deepcopy(row)
                    skip_row = False
                    for k, v in row_transformed.items():
                        try:
                            # Cast integers & floats
                            if re_fullmatch(DwcArchiveSection._REGEXP_INT, v) is not None:
                                row_transformed[k] = int(v)
                            elif re_fullmatch(DwcArchiveSection._REGEXP_FLOAT, v) is not None:
                                row_transformed[k] = float(v)
                            # Cast empty strings to None
                            elif v == "":
                                row_transformed[k] = None
                        except Exception as e:
                            warning = "Error parsing line {} of '{}'. It will be skipped.\n{}\n".format(
                                row_idx + 1,
                                f.name,
                                e
                            )
                            DwcArchiveSection._LOGGER.warning(warning)
                            skip_row = True

                    if not skip_row:
                        yield row_transformed
