from os.path import basename, split as path_split, join as path_join
from typing import Iterable, List
from zipfile import ZipFile
from ..meta._meta import DwcMeta
from ._section import DwcArchiveSection
from xml.etree import ElementTree


class DwcArchive:
    _FILE_DWC_META = "meta.xml"

    def __init__(self, archive_path: str, meta: DwcMeta):
        self._archive_path = archive_path
        self._meta = meta
        self._zip_workdir = ""

    @property
    def core(self) -> DwcArchiveSection:
        return DwcArchiveSection(self._meta.core)

    @property
    def extensions(self) -> List[DwcArchiveSection]:
        return [DwcArchiveSection(e) for e in self._meta.extensions]

    def iter_core_entries(self) -> Iterable[dict]:
        for file in self.core.files:
            file = path_join(self._zip_workdir, file)
            for line in self.core._iter_file(self._archive_path, file):
                yield line

    def iter_extension_entries(self, row_type: str) -> Iterable[dict]:
        extensions = filter(lambda e: e.row_type == row_type, self.extensions)
        for extension in extensions:
            for ext_file in extension.files:
                ext_file = path_join(self._zip_workdir, ext_file)
                for line in extension._iter_file(self._archive_path, ext_file):
                    yield line

    @staticmethod
    def _find_meta_file(archive_file_lst: List[str]) -> str:
        meta_files = [*filter(
            lambda file: basename(file) == DwcArchive._FILE_DWC_META, 
            archive_file_lst
        )]
        if len(meta_files) > 1:
            raise Exception("More than one meta file found in archive")
        return meta_files[0]

    @staticmethod
    def read(file_path: str) -> "DwcArchive":
        with ZipFile(file_path) as zf:
            # Find the directory where meta.xml exists
            meta_file = DwcArchive._find_meta_file(zf.namelist())
            zip_workdir = path_split(meta_file)[0]            

            with zf.open(meta_file, force_zip64=True) as f:
                meta_contents = f.read()

        tree = ElementTree.fromstring(meta_contents)
        archive = DwcArchive(file_path, DwcMeta.from_xml(tree))
        archive._zip_workdir = zip_workdir
        return archive
