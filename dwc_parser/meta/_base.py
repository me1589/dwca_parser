from abc import ABC as AbstractBaseClass, abstractmethod
from copy import copy
from typing import Dict, List
from xml.etree.ElementTree import Element


class DwcBase(AbstractBaseClass):
    _XMLNS_DWC_TEXT = {
        "dwc": "http://rs.tdwg.org/dwc/text/"
    }

    @abstractmethod
    def to_dict(self) -> Dict:
        return dict()

    @staticmethod
    @abstractmethod
    def from_xml(elem: Element):
        pass

    @abstractmethod
    def to_xml() -> Element:
        pass

    @staticmethod
    def _find_dwc_tags(element: Element, tag_name: str) -> List[Element]:
        return element.findall(tag_name, namespaces=DwcBase._XMLNS_DWC_TEXT)

    @staticmethod
    def _normalize_attrs(element: Element) -> Element:
        new_attrs = dict()
        for k, v in element.attrib.items():
            new_attrs[k.lower()] = v
        e = copy(element)
        e.attrib = new_attrs
        return e
