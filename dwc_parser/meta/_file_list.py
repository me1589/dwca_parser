from ._base import DwcBase
from typing import List, Dict
from xml.etree.ElementTree import Element, SubElement


class DwcFileList(DwcBase):
    _TAG_NAME_FILES = "dwc:files"
    _TAG_NAME_LOCATION = "dwc:location"

    def __init__(self, locations: List[str]):
        super().__init__()
        self.locations = locations

    def to_dict(self) -> Dict:
        return {
            "locations": self.locations
        }

    def to_xml(self) -> Element:
        list_elem = Element(DwcFileList._TAG_NAME_FILES)
        for location in self.locations:
            loc_elem = SubElement(list_elem, DwcFileList._TAG_NAME_LOCATION)
            loc_elem.text = location
        return list_elem

    @staticmethod
    def from_xml(elem: Element):
        location_elems = DwcFileList._find_dwc_tags(
            elem,
            DwcFileList._TAG_NAME_LOCATION, 
        )
        if len(location_elems) == 0:
            raise Exception("Invalid file element, no locations specified")
        
        locations = list()
        for elem in location_elems:
            if elem.text is None:
                raise Exception("Invalid location element, no inner text")
            locations.append(elem.text)

        return DwcFileList(locations)