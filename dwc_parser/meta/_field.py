from ._base import DwcBase
from typing import Optional, Dict
from xml.etree.ElementTree import Element


class DwcField(DwcBase):
    _TAG_NAME_FIELD = "dwc:field"

    _ATTR_TERM = "term"
    _ATTR_INDEX = "index"
    _ATTR_DEFAULT = "default"
    _ATTR_VOCAB = "vocabulary"
    _ATTRS = [
        _ATTR_TERM, 
        _ATTR_INDEX,
        _ATTR_DEFAULT,
        _ATTR_VOCAB,
    ]

    def __init__(self, term: str):
        super().__init__()
        self.term = term
        self.index: Optional[str] = None
        self.default: Optional[str] = None
        self.vocabulary: Optional[str] = None

    def to_dict(self) -> Dict[str, str]:
        as_dict = dict()
        for attr_name in DwcField._ATTRS:
            val = getattr(self, attr_name)
            if val is not None:
                as_dict[attr_name] = val
        return as_dict

    def to_xml(self) -> Element:
        elem = Element(DwcField._TAG_NAME_FIELD)
        for attr_name in DwcField._ATTRS:
            val = getattr(self, attr_name)
            if val is not None:
                elem.attrib[attr_name] = val
        return elem

    @staticmethod
    def from_xml(elem: Element):
        elem = DwcField._normalize_attrs(elem)
        if DwcField._ATTR_TERM.lower() not in elem.attrib.keys():
            raise Exception("Invalid field element, no term specified")
        
        field = DwcField(elem.attrib[DwcField._ATTR_TERM])

        attrs = [a.lower() for a in DwcField._ATTRS]
        for attr_name in attrs:
            if attr_name in elem.attrib.keys():
                setattr(field, attr_name, elem.attrib[attr_name])

        return field

