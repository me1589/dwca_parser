from typing import List, Dict
from ._file_list import DwcFileList
from ._base import DwcBase
from ._field import DwcField
from xml.etree.ElementTree import Element


class DwcFileMeta(DwcBase):
    _TAG_NAME_CORE = "dwc:core"
    _TAG_NAME_EXTENSION = "dwc:extension"
    _TAG_NAME_FILES = "dwc:files"
    _TAG_NAME_FIELD = "dwc:field"
    _TAG_NAME_ID = "dwc:id"
    _TAG_NAME_CORE_ID = "dwc:coreid"
    _ID_ATTR_IDX = "index"

    _ATTR_ROW_TYPE = "rowtype"
    _ATTR_FIELDS_TERM_BY = "fieldsTerminatedBy"
    _ATTR_LINES_TERM_BY = "linesTerminatedBy"
    _ATTR_FIELDS_ENCLOSED_BY = "fieldsEnclosedBy"
    _ATTR_ENCODING = "encoding"
    _ATTR_IGNORE_HEADER_LINES = "ignoreHeaderLines"
    _ATTR_DATE_FMT = "dateFormat"
    _ATTRS = [
        _ATTR_FIELDS_TERM_BY,
        _ATTR_LINES_TERM_BY,
        _ATTR_FIELDS_ENCLOSED_BY,
        _ATTR_ENCODING,
        _ATTR_IGNORE_HEADER_LINES,
        _ATTR_DATE_FMT
    ]

    def __init__(self, row_type: str, files: List[DwcFileList], fields: List[DwcField]):
        super().__init__()
        self.rowtype = row_type
        self.files = files
        self.fields = fields

        # Defaults
        self.id = None
        self.coreid = None
        self.fieldsTerminatedBy = ","
        self.linesTerminatedBy = "\n"
        self.fieldsEnclosedBy = '"'
        self.encoding = "UTF-8"
        self.ignoreHeaderLines = "0"
        self.dateFormat = "YYYY-MM-DD"

    def to_dict(self) -> Dict:
        as_dict = {
            "id": self.id,
            "coreid": self.coreid,
            "files": [f.to_dict() for f in self.files],
            "fields": [f.to_dict() for f in self.fields]
        }
        for attr_name in DwcFileMeta._ATTRS:
            as_dict[attr_name] = getattr(self, attr_name)
        return as_dict

    def to_xml(self) -> Element:

        # Default to a 'core' element if id is not set
        if self.coreid is not None:
            tag_name = DwcFileMeta._TAG_NAME_EXTENSION
        else:
            tag_name = DwcFileMeta._TAG_NAME_CORE

        elem = Element(tag_name)
        id_elem = None

        if self.id is not None:
            id_elem = Element(DwcFileMeta._TAG_NAME_ID)
            id_elem.attrib["index"] = self.id
        
        if self.coreid is not None:
            id_elem = Element(DwcFileMeta._TAG_NAME_CORE_ID)
            id_elem.attrib["index"] = self.coreid

        if id_elem is not None:
            elem.append(id_elem)

        [elem.append(fl.to_xml()) for fl in self.files]
        [elem.append(fld.to_xml()) for fld in self.fields]

        for attr_name in DwcFileMeta._ATTRS:
            val = getattr(self, attr_name)
            if val is not None:
                elem.attrib[attr_name] = val

        return elem

    @staticmethod
    def from_xml(element: Element):
        element = DwcFileMeta._normalize_attrs(element)

        if DwcFileMeta._ATTR_ROW_TYPE.lower() not in element.attrib.keys():
            raise Exception("Missing rowtype in file meta")

        rowtype = element.attrib[DwcFileMeta._ATTR_ROW_TYPE]

        file_elements = DwcFileMeta._find_dwc_tags(
            element,
            DwcFileMeta._TAG_NAME_FILES, 
        )
        if len(file_elements) != 1:
            raise Exception("Found {} != 1 files elements".format(len(file_elements)))

        field_elements = DwcFileMeta._find_dwc_tags(
            element,
            DwcFileMeta._TAG_NAME_FIELD
        )
        if len(field_elements) == 0:
            raise Exception("No fields defined")

        files = [DwcFileList.from_xml(f) for f in file_elements]
        fields = [DwcField.from_xml(f) for f in field_elements]

        file_meta = DwcFileMeta(rowtype, files, fields)

        for attr_name in DwcFileMeta._ATTRS:
            if attr_name.lower() in element.attrib.keys():
                setattr(file_meta, attr_name, element.attrib[attr_name.lower()])

        ids = DwcFileMeta._find_dwc_tags(
            element,
            DwcFileMeta._TAG_NAME_ID 
        )
        coreids = DwcFileMeta._find_dwc_tags(
            element,
            DwcFileMeta._TAG_NAME_CORE_ID 
        )
        
        if len(ids) > 1:
            raise Exception("id element is specified twice")
        if len(coreids) > 1:
            raise Exception("coreid element is specified twice")
        if len(coreids) == 1 and len(ids) == 1:
            raise Exception("Both coreid and id are specified")
        elif len(coreids) == 1:
            coreid_element = coreids[0]
            if DwcFileMeta._ID_ATTR_IDX in coreid_element.attrib.keys():
                file_meta.coreid = coreid_element.attrib[DwcFileMeta._ID_ATTR_IDX]
            else:
                raise Exception("Index attribute not set on coreid")

            # GBIF doesn't specify a <field> for the coreid
            if file_meta.coreid not in [f.index for f in file_meta.fields]:
                field = DwcField("coreid")
                field.index = file_meta.coreid
                file_meta.fields.append(field)

        elif len(ids) == 1:
            id_element = ids[0]
            if DwcFileMeta._ID_ATTR_IDX in id_element.attrib.keys():
                file_meta.id = id_element.attrib[DwcFileMeta._ID_ATTR_IDX]
            else:
                raise Exception("Index attribute not set on id")

            # GBIF doesn't specify a <field> for the id
            if file_meta.id not in [f.index for f in file_meta.fields]:
                field = DwcField("id")
                field.index = file_meta.id
                file_meta.fields.append(field)

        return file_meta