from typing import Dict, List
from xml.etree.ElementTree import Element
from ._base import DwcBase
from ._file import DwcFileMeta

# https://dwc.tdwg.org/text/


class DwcMeta(DwcBase):
    _TAG_NAME_ARCHIVE = "dwc:archive"
    _TAG_NAME_CORE = "dwc:core"
    _TAG_NAME_EXTENSION = "dwc:extension"

    def __init__(self, core: DwcFileMeta):
        super().__init__()
        self.core: DwcFileMeta = core
        self.extensions: List[DwcFileMeta] = list()

    def to_dict(self) -> Dict:
        return {
            "core": self.core.to_dict(),
            "extensions": [e.to_dict() for e in self.extensions]
        }

    def to_xml(self) -> Element:
        elem = Element(DwcMeta._TAG_NAME_ARCHIVE)
        for k, v in DwcMeta._XMLNS_DWC_TEXT.items():
            elem.attrib["xmlns:{}".format(k)] = v

        elem.append(self.core.to_xml())
        [elem.append(e.to_xml()) for e in self.extensions]
        return elem      

    @staticmethod
    def from_xml(element: Element):
        # This is hacky, we need a better way to make sure the root tag is 
        # 'archive' in the dwc text namespace
        if not element.tag.endswith("archive"):
            raise Exception("No archive element found: {}".format(element.tag))

        core_elements = DwcFileMeta._find_dwc_tags(
            element,
            DwcMeta._TAG_NAME_CORE 
        )
        extension_elements = DwcFileMeta._find_dwc_tags(
            element,
            DwcMeta._TAG_NAME_EXTENSION
        )

        if len(core_elements) != 1:
            raise Exception("Found {} != 1 core elements".format(len(core_elements)))

        core_element = core_elements[0]
        core = DwcFileMeta.from_xml(core_element)
        extensions = [DwcFileMeta.from_xml(e) for e in extension_elements]

        if len(extensions) > 0 and core.id is None:
            raise Exception("id is not set on core element")

        for e in extensions:
            if e.coreid is None:
                raise Exception("coreid is not set on one or more extension element")

        meta = DwcMeta(core)
        meta.extensions = extensions
        return meta
